package tk.vinemarques.lab.cotilmonitorias;

import android.app.Activity;
import android.os.Bundle;

public class ConfigActivity extends Activity {
    private ConfigFragment configFrag;


    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        configFrag = new ConfigFragment();

        getFragmentManager()
                .beginTransaction()
                .replace(android.R.id.content,configFrag,"configFragment")
                .commit();
    }
}
