package tk.vinemarques.lab.cotilmonitorias;


import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;

public class ConfigFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener {
    private SwitchPreference push;
    private SwitchPreference email;
    private ListPreference alarm;
    private SwitchPreference vibrate;

    private Resources res;
    private static final String KEY_APP_VERSION = "pref_app_version";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.config_layout);
        findPreference(KEY_APP_VERSION).setSummary(BuildConfig.VERSION_NAME);

        res = getActivity().getResources();

        push = (SwitchPreference) findPreference("push");
        email = (SwitchPreference) findPreference("email");
        alarm = (ListPreference) findPreference("alarm");
        vibrate = (SwitchPreference) findPreference("vibrate");

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        prefs.registerOnSharedPreferenceChangeListener(this);
        boolean pushDefaultValue = res.getBoolean(R.bool.push_default);

        Boolean enabled = prefs.getBoolean("push", pushDefaultValue);
        Habilita(enabled);
    }
    private void Habilita(Boolean enabled){
        alarm.setEnabled(enabled);
        vibrate.setEnabled(enabled);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        if(s.equals(push.getKey())){
            boolean pushDefaultValue = res.getBoolean(R.bool.push_default);
            boolean enabled = sharedPreferences.getBoolean(s,pushDefaultValue);
            Habilita(enabled);
        }
    }
}
