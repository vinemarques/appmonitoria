package tk.vinemarques.lab.cotilmonitorias;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.*;

public class LoginActivity extends MainActivity{

    private AutoCompleteTextView autoUser;
    private EditText edtSenha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        autoUser = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView);
        edtSenha = (EditText) findViewById(R.id.editText2);
    }

    public void onBtnClick(View botao) {
        String user = String.valueOf(autoUser.getText().toString());
        String pass = String.valueOf(edtSenha.getText().toString());

        LoginServiceTask task = new LoginServiceTask();
        task.execute(user, pass);
    }

    private class LoginServiceTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... strings) {
            String user = strings[0];
            String pass = strings[1];
            return callWebServicePost(user,pass);
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try{
                JSONObject aluno = new JSONObject(s);
                getApplicationContext().startActivity(new Intent(getApplicationContext(),MainActivity.class));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private String callWebServicePost(String user, String pass) {
            try {
                URL url = new URL("http://lab.vinemarques.tk/infd35/cotil/monitoria/assets/api/login.php");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                //conn.setReadTimeout(15000);
                //conn.setConnectTimeout(15000);
                conn.setDoInput(true);
                conn.setRequestMethod("POST");
                OutputStream os = conn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                StringBuilder result = new StringBuilder();
                result.append(URLEncoder.encode("catchup", "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(user, "UTF-8"));
                result.append("&");
                result.append(URLEncoder.encode("batatinha", "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(pass, "UTF-8"));
                writer.write(result.toString());
                writer.flush();
                writer.close();
                os.close();
                int responseCode = conn.getResponseCode();
                String response = "";
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    String line;
                    BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    while ((line = br.readLine()) != null) {
                        response += line;
                    }
                } else {
                    response = "";
                }
                return response;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
