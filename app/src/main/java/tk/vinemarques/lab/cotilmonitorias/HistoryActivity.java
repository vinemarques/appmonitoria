package tk.vinemarques.lab.cotilmonitorias;

import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

public class HistoryActivity extends AppCompatActivity{

    private String m_Text = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
    }

    public void onRadioButtonClicked(View view){
        boolean checked = ((RadioButton) view).isChecked();
        switch (view.getId()){
            case R.id.rbAll: {
                if (checked) {
                }
                break;
            }
            case R.id.rbNome: {
                if (checked) {
                    callDialog(1);
                }
                break;
            }
            case R.id.rbRa: {
                if (checked) {
                    callDialog(2);
                }
                break;
            }
            case R.id.rbData: {
                if (checked) {
                    callDialog(3);
                }
                break;
            }
            case R.id.rbMateria: {
                if (checked) {
                    callDialog(4);
                }
                break;
            }
        }
    }

    public void callDialog(int i){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        switch (i){
            case 1: builder.setTitle("Nome");
                break;
            case 2: builder.setTitle("RA");
                break;
            case 3: builder.setTitle("Data");
                break;
            case 4: builder.setTitle("Matéria");
                break;
        }


        final EditText input = new EditText(this);
        switch (i){
            case 1: input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PERSON_NAME);
                break;
            case 2: input.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_VARIATION_NORMAL);
                break;
            case 3: input.setInputType(InputType.TYPE_CLASS_DATETIME | InputType.TYPE_DATETIME_VARIATION_DATE);
                break;
            case 4: input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
                break;
        }

        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                m_Text = input.getText().toString();
            }
        });

        builder.show();
    }
}
